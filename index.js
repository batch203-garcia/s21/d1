//console.log("Hello World!")

//list all student ID for all graduating students.

let studentNumberA = "2020-1923";
let studentNumberB = "2020-1924";
let studentNumberC = "2020-1925";
let studentNumberD = "2020-1926";
let studentNumberE = "2020-1927";

//We can simply write the code above like this in array
let studentNumbers = ["2020-1923", "2020-1924", "2020-1925", "2020-1926", "2020-1927", ];

//[SECTION] Arrays
//Arrays -> used to store multiple related values -> enclosed with square brackets -> objects or values are called array literals -> Syntax:

let grades = [98.5, 94.3, 89.2, 90];
let computerBrands = ["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "Fujitsu"];

console.log(grades);
console.log(computerBrands);

//possible use of array but not recommended
let mixedArr = ["John", "Doe", 12, false, null, undefined, {}];
console.log(mixedArr);

//Alternative way to write array

let myTasks = [
    "Drink HTML",
    "Eat JavaScript",
    "Inhale CSS",
    "Bake Sass"
];

console.log(myTasks);

//creating array with values for variable

let city1 = "Tokyo"
let city2 = "Manila"
let city3 = "Jakarta"

let cities = [city1, city2, city3];

console.log(cities);

//Object values; let sampleObjects = [{obj1}, {obj2}, {obj3}];

//[SECTION] .length property

//.length -> get and set the total number of items in an array

console.log(myTasks.length);
console.log(cities.length);

let blankArr = [];
console.log(blankArr.length); //.length -> can be used with strings -> count all values

let fullName = "Romenick Garcia";

console.log(fullName.length); //.length -> counts all char even spaces

//.length -> can set total number of items in an array

console.log(myTasks);
myTasks.length = myTasks.length - 1;
console.log(myTasks.length);
console.log(myTasks); //.length -> will work in values

//To delete a specific item in an array -> employ array methods.

//using decrement

cities.length--;
console.log(cities);

fullName.length = fullName.length - 1;
console.log(fullName.length);
console.log(fullName); //.length -> wont work in strings

//increase array length
let theBeatles = ["John", "Paul", "Ringo", "George"];
//theBeatles.length++;
//console.log(theBeatles);

theBeatles[theBeatles.length] = "Romenick";
console.log(theBeatles);

console.log(grades[0]);
console.log(computerBrands[3]);

console.log(grades[20]); //does not exist -> undefined

let lakersLegends = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem"];
console.log(lakersLegends[1]); //Shaq
console.log(lakersLegends[3]); //Magic

// can save/store items in another variable

let currentLaker = lakersLegends[2];
console.log(currentLaker);

//re-assign array values using item index
console.log("Array before re-assignment");
console.log(lakersLegends);

lakersLegends[2] = "Gasol";
console.log("Array after re-assignment");
console.log(lakersLegends);

//Access the last element
let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];
let lastElemetIndex = bullsLegends.length - 1;
console.log(bullsLegends[lastElemetIndex]);

//Add items in an array using indeces

const newArr = [];
console.log(newArr[0]);

newArr[0] = "Cloud Strife";
console.log(newArr);

console.log(newArr[1]);
newArr[1] = "Tifa Lockhart";
console.log(newArr);

//we can add item at the end of array using .length

newArr[newArr.length] = "Barrett Wallace";
console.log(newArr);

//looping over an array

for (let index = 0; index < newArr.length; index++) {
    //able to show each array items in clg
    console.log(newArr[index]);
}

//create a program that will filter the array of numbers

let numArr = [5, 12, 30, 46, 40, 52];

for (let i = 0; i < numArr.length; i++) {
    if (numArr[i] % 5 === 0) {
        console.log(numArr[i] + " is divisible by 5");
    } else {
        console.log(numArr[i] + " is not divisible by 5");
    }
}

//[SECTION] - Multidimensional Array
//used for storing complex data structures -> visualize real wolrd objects -> math computations, img processing, record management -> Array within an array

let chessBoard = [
    ["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"],
    ["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"],
    ["a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"],
    ["a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4"],
    ["a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5"],
    ["a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6"],
    ["a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"],
    ["a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8"],
]

console.log(chessBoard);